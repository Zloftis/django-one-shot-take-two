from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm


def todo_list_list(request):
    tod_lists = TodoList.objects.all()
    context = {
        "todo_list_list": tod_lists,
    }
    return render(request, "todos/main-page.html", context)


def todo_list_detail(request, id):
    todo_item = get_object_or_404(TodoList, id=id)
    context = {
        "list_details": todo_item,
    }
    return render(request, "todos/details.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()
    context = {
        "form": form
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    edit = get_object_or_404(TodoList, id=id)

    if request.method == "Post":
        form = TodoForm(request.Post, Instance=edit)
        if form.is_valid():
            edit = form.save()
            return redirect("todo_list_detail", id=edit.id)

    else:
        form = TodoForm(instance=edit)

    context = {
        "form": form
    }
    return render(request, "todos/update.html", context)

# Create your views here.
